package com.ionix.prueba.user.mapper;

import com.ionix.prueba.user.dto.UserDTO;
import com.ionix.prueba.user.dto.UserRequestDTO;
import com.ionix.prueba.user.entity.User;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class UserMapper {
    private UserMapper(){}

    public static User toEntity(UserDTO dto){
        User user=new User();
        user.setId(dto.getId());
        user.setName(dto.getName());
        user.setUsername(dto.getUsername());
        user.setEmail(dto.getEmail());
        user.setPhone(dto.getPhone());
        return user;
    }

    public static User requestToEntity(UserRequestDTO dto){
        User user=new User();

        if(StringUtils.hasText(dto.getName())) {
            user.setName(dto.getName());
        }

        if(StringUtils.hasText(dto.getUsername())) {
            user.setUsername(dto.getUsername());
        }

        if(StringUtils.hasText(dto.getEmail())) {
            user.setEmail(dto.getEmail());
        }

        if(StringUtils.hasText(dto.getPhone())) {
            user.setPhone(dto.getPhone());
        }

        return user;
    }


    public static User requestToEntity(User user,UserRequestDTO dto){

        if(StringUtils.hasText(dto.getName())) {
            user.setName(dto.getName());
        }

        if(StringUtils.hasText(dto.getUsername())) {
            user.setUsername(dto.getUsername());
        }

        if(StringUtils.hasText(dto.getEmail())) {
            user.setEmail(dto.getEmail());
        }

        if(StringUtils.hasText(dto.getPhone())) {
            user.setPhone(dto.getPhone());
        }

        return user;
    }
}
