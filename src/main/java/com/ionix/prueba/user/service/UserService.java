package com.ionix.prueba.user.service;

import com.ionix.prueba.user.dto.CifradoResponseDTO;
import com.ionix.prueba.user.dto.UserRequestDTO;
import com.ionix.prueba.user.entity.User;
import com.ionix.prueba.user.mapper.UserMapper;
import com.ionix.prueba.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Value("${sharedKey}")
    private String sharedKey;

    @Value("${apiKey}")
    private String apiKey;

    @Value("${api.url}")
    private String apiUrl;

    public CifradoResponseDTO getDataByExternalApi(String key) {
        try {
            CifradoResponseDTO cifradoResponseDTO = new CifradoResponseDTO();
            String encryptedRut = encodeDES(key);
            System.out.println(encryptedRut);
            cifradoResponseDTO.setKey(encryptedRut);
            String response=callExternalAPI(encryptedRut);
            System.out.println(response);
            return cifradoResponseDTO;
        } catch (Exception e) {
            return null;
        }
    }

    public User create(UserRequestDTO dto) {
        try {
            User oldUser = UserMapper.requestToEntity(dto);
            User user = userRepository.save(oldUser);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<User> findAll(int page, int size, String search) {
        try {
            List<User> userList = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size);
            userRepository.findAll((Specification<User>) (root, query, criteriaBuilder) -> {
                List<Predicate> predicates = new ArrayList<>();
                if (StringUtils.hasText(search)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("name"), "%" + search + "%")));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }, paging).forEach(i -> userList.add(i));
            return userList;
        } catch (Exception e) {
            return null;
        }
    }

    public User findByEmail(String search) {
        try {
            User user = userRepository.findByEmail(search).orElseThrow(() -> new Exception("user no found"));
            return user;
        } catch (Exception e) {
            return null;
        }
    }

    public User findById(String id) {
        try {
            User user = userRepository.findById(Long.valueOf(id)).orElseThrow(() -> new Exception("user no found"));
            return user;
        } catch (Exception e) {
            return null;
        }
    }

    public User update(Long id, UserRequestDTO dto) {
        try {

            Optional<User> oldUser = userRepository.findById(id);
            if (!oldUser.isPresent()) {
                throw new Exception("student not found");
            }
            User user = oldUser.get();
            user = UserMapper.requestToEntity(user, dto);
            user = userRepository.save(user);
            return user;
        } catch (Exception e) {
            return null;
        }
    }

    public void deleteById(Long id) {
        Optional<User> oldUser = userRepository.findById(id);
        if (!oldUser.isPresent()) {
            try {
                throw new Exception("student not found");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        userRepository.deleteById(id);
    }

    private String callExternalAPI(String param) {
        String responseBody = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add("X-API-Key", apiKey);
            HttpEntity<Void> requestEntity = new HttpEntity<>(headers);
            String url = apiUrl.replace("{parameter}", param);
            System.out.println(url);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
            responseBody = response.getBody();
            System.out.println(responseBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseBody;
    }

    private String encodeDES(String key) {
        try {
            DESKeySpec keySpec = new DESKeySpec(sharedKey.getBytes("UTF-8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            byte[] cleartext = key.getBytes("UTF-8");
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, keyFactory.generateSecret(keySpec));
            Base64.Encoder encoder = Base64.getEncoder();
            String encryptedRut = encoder.encodeToString(cipher.doFinal(cleartext));
            return encryptedRut;
        } catch (Exception e) {
            return null;
        }
    }
}
