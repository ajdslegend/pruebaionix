package com.ionix.prueba.user.controller;

import com.ionix.prueba.user.dto.CifradoRequestDTO;
import com.ionix.prueba.user.dto.CifradoResponseDTO;
import com.ionix.prueba.user.dto.UserRequestDTO;
import com.ionix.prueba.user.entity.User;
import com.ionix.prueba.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/user")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/cipher")
    public CifradoResponseDTO cifra(@RequestBody CifradoRequestDTO request){
        return userService.getDataByExternalApi(request.getKey());
    }

    @PostMapping("")
    public ResponseEntity<User> create(@RequestBody UserRequestDTO request) {
        User entity = userService.create(request);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> update(@PathVariable("id")Long id,@RequestBody UserRequestDTO request) {
        User entity = userService.update(id,request);
        return new ResponseEntity<>(entity, HttpStatus.ACCEPTED);
    }

    @GetMapping("/search-by-email/{search}")
    public ResponseEntity<User> findByEmail(@PathVariable("search") String search) {
        User user = userService.findByEmail(search);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> findById(@PathVariable("id") String search) {
        User user = userService.findById(search);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) {
         userService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("")
    public ResponseEntity<List<User>> find(@RequestParam(required = false) int page,
                                           @RequestParam(required = false) int size,
                                           @RequestParam(required = false) String search) {
        List<User> list = userService.findAll(page, size, search);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
